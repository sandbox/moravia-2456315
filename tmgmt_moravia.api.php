<?php

/**
 * @file
 * File with api call function.
 *
 * Url: http://www.moravia.com/
 */

/**
 * Function for communicating with Symfonie api.
 *
 * @param TMGMTTranslator $translator
 *   TMGMTTranslator instance.
 * @param string $url
 *   Method url.
 * @param string $method
 *   Request method.
 * @param mixed $data
 *   Request data.
 * @param bool $json_decode
 *   Decode the response data json.
 *
 * @return object
 *   Request result.
 */
function tmgmt_moravia_symfonie_api_call(TMGMTTranslator $translator, $url, $method, $data = NULL, $json_decode = TRUE) {
  $domain = "https://projects.moravia.com";

  // Default call values.
  $user_options = array();
  $content_type = "application/json; charset=UTF-8";
  $options = array(
    "method" => $method,
    "headers" => array(
      "Authorization" => "authToken " . $translator->getSetting('symfonie_two_token'),
      "Content-Type" => "$content_type",
    ),
  );

  if ($data) {
    $options['data'] = $data;
  }

  // Call the API.
  $response = drupal_http_request($domain . $url, $options);

  // Check response state and write it to Drupal log.
  if (isset($response->error) && ($response->code != 200) && ($response->code != 201) && ($response->code != 204)) {
    watchdog('moravia_symfonie_two', $response->error . ' ' . $method . ' ' . $url, array(), WATCHDOG_ERROR);
  }
  else {
    watchdog('moravia_symfonie_two', $method . ' ' . $url, array(), WATCHDOG_DEBUG);
  }

  // Decode json data.
  if ($json_decode) {
    $data = json_decode($response->data);
    $response->data = $data;
  }

  /*
   * Odata API pagianation, if there is next page link,
   * recursively call again and append results.
   */
  $next_link_key = "@odata.nextLink";
  if (isset($response->data->$next_link_key) && $response->data->$next_link_key) {
    $next_link = substr($response->data->$next_link_key, strlen($domain));
    $next_page = tmgmt_moravia_symfonie_api_call($translator, $next_link, $method, $options['data']);

    if ($next_page->data->value) {
      foreach ($next_page->data->value as $val) {
        $response->data->value[] = $val;
      }
    }
    else {
      watchdog('moravia_symfonie_two', $response->error . ' ' . $method . ' ' . $url, array(), WATCHDOG_ERROR);
    }
  }

  return $response;
}
