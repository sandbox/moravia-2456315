<?php

/**
 * @file
 * Module file of the TMGMT Moravia Two module.
 */

require_once 'tmgmt_moravia.download.php';
require_once 'tmgmt_moravia.ui.php';

/**
 * Implements hook_tmgmt_translator_plugin_info().
 */
function tmgmt_moravia_tmgmt_translator_plugin_info() {
  return array(
    'moravia_two' => array(
      'label' => t('Moravia translator 2'),
      'description' => t('Moravia translator service for Symfonie 2.'),
      'plugin controller class' => 'TMGMTMoraviaPluginController',
      'ui controller class' => 'TMGMTMoraviaTranslatorUIController',
      'auto create' => TRUE,
    ),
  );
}

/**
 * Check periodically translation status, if there are any unfinished jobs.
 */
function tmgmt_moravia_cron() {
  tmgmt_moravia_download();
}

/**
 * Generate friendly URL.
 *
 * @param string $str
 *   Dirty url.
 *
 * @return string
 *   Friendly clean URL.
 */
function tmgmt_moravia_url_friendly_string($str) {
  // Convert spaces to '-', remove characters that are not alphanumeric.
  // Or a '-', combine multiple dashes (i.e., '---') into one dash '-'.
  $str = preg_replace("#[-]+#", "-", preg_replace("#[^a-z0-9-]#", "",
    strtolower(str_replace(" ", "-", $str))));
  return $str;
}

/**
 * Creates an unique HTML file to export node attribute to Symfonie 2.
 *
 * @param int $tjiid
 *   Job item tjiid.
 * @param string $label
 *   Job name label.
 * @param string $key
 *   Translated field key (same as id attribute in xliff file).
 * @param string $target_lang_code
 *   Target language code.
 *
 * @return string
 *   Unique HTML file path.
 */
function tmgmt_moravia_create_file_name($tjiid, $label, $key, $target_lang_code) {
  return $tjiid . "_" . tmgmt_moravia_url_friendly_string($label) . "_" . str_replace("][", "-", $key) . "_" . $target_lang_code . ".html";
}
