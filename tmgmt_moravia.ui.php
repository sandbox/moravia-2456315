<?php

/**
 * @file
 * File for TMGMT Moravia Two translator UI controller.
 */

/**
 * TMGMT TMGMT Moravia Two translator UI controller.
 */
class TMGMTMoraviaTranslatorUIController extends TMGMTDefaultTranslatorUIController {
  /**
   * Get form settings.
   *
   * @param array $form
   *   Form to draw UI.
   * @param object $form_state
   *   State of form.
   * @param TMGMTTranslator $translator
   *   Instance of translator.
   * @param bool $busy
   *   Busy status.
   *
   * @return mixed
   *   Return form settings.
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {

    $form['symfonie_two_token'] = array(
      '#type' => 'textfield',
      '#title' => t('Moravia Symfonie Token'),
      '#default_value' => $translator->getSetting('symfonie_two_token'),
      '#description' => t('Please enter Authentication Token from Moravia Symfonie application'),
    );
    return parent::pluginSettingsForm($form, $form_state, $translator, $busy);
  }

  /**
   * Get task name.
   *
   * @param TMGMTJob $job
   *   Job.
   *
   * @return string
   *   Task name.
   */
  private function getDefaultTaskName(TMGMTJob $job) {
    return $job->label;
  }

  /**
   * Check form settings.
   *
   * @param array $form
   *   Form to draw UI.
   * @param object $form_state
   *   State of form.
   * @param TMGMTJob $job
   *   Instance of TMGMTJob.
   *
   * @return mixed
   *   Status of form.
   */
  public function checkoutSettingsForm($form, &$form_state, TMGMTJob $job) {
    /* @noinspection PhpUndefinedMethodInspection */
    $form['moravia_project'] = array(
      '#type' => 'select',
      '#title' => t('Symfonie project'),
      '#options' => $job->getTranslator()->getController()->getProjects($job->getTranslator()),
      '#default_value' => $job->getTranslator()->getSetting('export_format'),
      '#description' => t('Please select a Symfonie project.'),
    );

    $form['moravia_task_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Task name'),
      '#default_value' => $this->getDefaultTaskName($job),
      '#description' => t('Please select a task name'),
      '#size' => 160,
    );

    $form['moravia_task_due_date'] = array(
      '#type' => 'fieldset',
      '#title' => format_string("@deadline - @usertimezone @timezone", array('@deadline' => t('Deadline'), '@usertimezone' => drupal_get_user_timezone(), '@timezone' => t(" timezone"))),
    );

    $form['moravia_task_due_date']['date'] = array(
      '#type' => 'date',
      '#title' => t('Due date'),
      '#default_value' => "",
      '#description' => t('Please select a deadline'),
    );

    $hours_options = array();

    foreach (range(0, 23) as $hour) {
      $hours_options[$hour] = "$hour:00";
    }

    $form['moravia_task_due_date']['time'] = array(
      '#title' => t('Due time'),
      '#default_value' => 17, //default due time set to 17:00
      '#type' => 'select',
      '#options' => $hours_options,
    );

    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

}
