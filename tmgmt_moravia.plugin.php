<?php

/**
 * @file
 * Plugin for communication with Symfonie API.
 *
 * Url: http://www.moravia.com/
 */

require_once 'tmgmt_moravia.api.php';
require_once 'tmgmt_moravia.task.php';

/**
 * Translator plugin container.
 */
class TMGMTMoraviaPluginController extends TMGMTDefaultTranslatorPluginController {

  /**
   * Checks whether a translator is available.
   *
   * @param TMGMTTranslator $translator
   *   The translator entity.
   *
   * @return bool
   *   TRUE if the translator plugin is available, FALSE otherwise.
   */
  public function isAvailable(TMGMTTranslator $translator) {
    if ($translator->getSetting('symfonie_two_token')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Settings checkout.
   *
   * @param TMGMTJob $job
   *   TMGMTJob instance.
   *
   * @return bool
   *   Result of check.
   */
  public function hasCheckoutSettings(TMGMTJob $job) {
    return TRUE;
  }

  /**
   * Returns project list.
   *
   * @param TMGMTTranslator $translator
   *   TMGMTTranslator instance.
   *
   * @return array
   *   Project list.
   */
  public function getProjects(TMGMTTranslator $translator) {
    $result = tmgmt_moravia_symfonie_api_call($translator, "/api/Projects/", "GET");
    $projects_result = $result->data;
    $projects = array();
    foreach ($projects_result->value as $project) {
      $projects[$project->Id] = $project->Name;
    }
    return $projects;
  }

  /**
   * Send translate task to Symfonie.
   *
   * @param TMGMTJob $job
   *   TMGMTJob instance.
   *
   * @return TMGMTMoraviaSymfonieTask
   *   TMGMTMoraviaSymfonieTask instance.
   */
  public function jobToSymfonieTask(TMGMTJob $job) {
    $task = TMGMTMoraviaSymfonieTask::load($job->getTranslator(), $job->reference);
    $task->job = $job;
    return $task;
  }

  /**
   * Submits the translation request and sends it to the translation provider.
   *
   * @param TMGMTJob $job
   *   The job that should be submitted.
   *
   * @ingroup tmgmt_remote_languages_mapping
   */
  public function requestTranslation(TMGMTJob $job) {
    $task = TMGMTMoraviaSymfonieTask::createFromJob($job);

    if ($task) {
      $task->save($job->getTranslator(), $job);
      $job->reference = $task->getId();

      foreach ($job->getItems() as $item) {
        $item->addRemoteMapping('Symfonie', 'Remote');
      }

      $job->submitted();
      $job->save();
    }
  }

  /**
   * Cancel translation task.
   *
   * @param TMGMTJob $job
   *   TMGMTJob instance.
   *
   * @return bool
   *   Result of cancel request.
   */
  public function abortTranslation(TMGMTJob $job) {
    $translator = $job->getTranslator();
    tmgmt_moravia_add_comment($translator, $job->reference, t("Drupal job was aborted/deleted by owner, please contact your point of contact for more information."));

    $job->aborted();

    return TRUE;
  }

}
