<?php

/**
 * @file
 * Create task for translation.
 *
 * Url: http://www.moravia.com/
 */

require_once 'tmgmt_moravia.upload.php';

/**
 * Symfonie task abstraction.
 */
class TMGMTMoraviaSymfonieTask {
  protected $title;
  protected $project;
  protected $sourceLanguageId;
  protected $targetLanguageId;
  protected $sourceLanguageCode;
  protected $targetLanguageCode;
  protected $dueDate;
  protected $stateId;
  protected $id = NULL;
  protected $xliff;
  protected $dueDateEn;

  public $attachments = array();
  public $job = NULL;

  /**
   * Set title.
   *
   * @param string $title
   *   Task title.
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Get state ID.
   *
   * @return int
   *   Id.
   */
  public function getStateId() {
    return $this->stateId;
  }

  /**
   * Create task from job.
   *
   * @param TMGMTJob $job
   *   TMGMTJob job.
   *
   * @return TMGMTMoraviaSymfonieTask
   *   Symfonie task.
   */
  public static function createFromJob(TMGMTJob $job) {
    $task = new TMGMTMoraviaSymfonieTask();

    // Job reference is equal to ID of remote symfonie task.
    if ($job->reference) {
      $task->id = $job->reference;
    }

    $translator = $job->getTranslator();

    $task->title = $job->settings['moravia_task_name'];
    $task->project = $job->settings['moravia_project'];
    $due_date = $job->settings['moravia_task_due_date']['date']['year'] . '-' .
        $job->settings['moravia_task_due_date']['date']['month'] . '-' .
        $job->settings['moravia_task_due_date']['date']['day'] . ' ' .
        $job->settings['moravia_task_due_date']['time'] . ':00:00';
    $due_date = gmdate('Y-m-d\TH:i:s\Z', strtotime($due_date));
    $task->dueDateEn = gmdate('Y-m-d H:i:s', strtotime($due_date));
    $task->dueDate = $due_date;

    $task->sourceLanguageCode = $translator->settings['remote_languages_mappings'][$job->source_language];
    $task->targetLanguageCode = $translator->settings['remote_languages_mappings'][$job->target_language];

    if (!$task->sourceLanguageCode) {
      $job->rejected(t('Source language mapping problem.'), array(), 'error');
      return FALSE;
    }
    if (!$task->targetLanguageCode) {
      $job->rejected(t('Target language mapping problem.'), array(), 'error');
      return FALSE;
    }

    $task->job = $job;
    return $task;
  }

  /**
   * Get ID.
   *
   * @return int
   *   Id.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Checking posibility adding language and files to existing job.
   *
   * Check if its possible to add new language and upload files to existing
   * Symfonie 2 job or we must create new one.
   *
   * @param TMGMTJob $job
   *   Instance of TMGMTJob.
   *
   * @return bool
   *   If is possible to add new language.
   */
  protected function checkAvailableSymJob(TMGMTJob $job) {
    $items = $job->getItems();
    /*
     * It must be only one node to translate,
     * or create new Sym 2 job every time.
     */
    if (count($items) > 1) {
      return FALSE;
    }

    $item = reset($items);

    $return = FALSE;
    if ($item) {
      // Check if there is the record for this node, or create new Sym 2 job.
      $result = db_query("SELECT * FROM moravia_two_external_pairs WHERE item_id=:id AND item_type=:type AND project_id=:p_id", array(
          ':id' => $item->item_id,
          ':type' => $item->item_type,
          ':p_id' => $job->settings['moravia_project'],
      ));
      $row = $result->fetchAssoc();

      if ($row) {
        // Get the Sym 2 job data.
        $ex_job = tmgmt_moravia_symfonie_api_call($job->getTranslator(), "/api/Jobs({$row['job_id']})", "GET");
        $ex_job_data = $ex_job->data;

        // Check job status.
        if ($ex_job_data->State != 'Draft' && $ex_job_data->State != 'Order') {
          return FALSE;
        }

        // Get all file names, that will be created.
        $items = $job->getItems();
        $file_names = array();
        if ($items) {
          foreach ($items as $item) {
            $data = array_filter(tmgmt_flatten_data($item->getData()), '_tmgmt_filter_data');
            foreach ($data as $key => $element) {
              $file_names[] = tmgmt_moravia_create_file_name($item->tjiid, $job->label, $key, $this->targetLanguageCode);
            }
          }
        }

        // Check if there are already no files with the same names in the job.
        $ex_job_attach = tmgmt_moravia_symfonie_api_call($job->getTranslator(), '/api/JobAttachments?$filter=' . urlencode('JobId eq ' . $row['job_id']), "GET");
        $ex_job_attach_data = $ex_job_attach->data;
        $return = $row['job_id'];

        foreach ($ex_job_attach_data->value as $file) {
          if (in_array($file->Name, $file_names)) {
            return FALSE;
          }
        }
      }
    }

    return $return;
  }

  /**
   * Save task.
   *
   * @param TMGMTTranslator $translator
   *   Translator instance.
   * @param TMGMTJob $job
   *   Job instance.
   *
   * @throws Exception
   *   Error during saving task.
   */
  public function save(TMGMTTranslator $translator, TMGMTJob $job) {
    $desc = "";

    // If there is already awailable job in the Sym 2.
    if ($this->id = $this->checkAvailableSymJob($job)) {
      $result = tmgmt_moravia_symfonie_api_call($translator, "/api/Jobs({$this->id})", "GET");
      $job_data = $result->data;

      $desc = $job_data->Description;

      $request_array = array();
      $request_array['Id'] = $this->id;
      // Add the required target language to the Sym 2 job.
      $job_data->TargetLanguageCodes[] = $this->targetLanguageCode;
      $request_array['TargetLanguageCodes'] = $job_data->TargetLanguageCodes;

      // Change the Sym 2 job duedate to the latest one.
      if ($this->dueDate > $job_data->DueDate) {
        $request_array['DueDate'] = $this->dueDate;
      }

      // Update sym 2 job data.
      tmgmt_moravia_symfonie_api_call($translator, "/api/Jobs(" . $this->id . ")", "PATCH", json_encode($request_array));
    }
    else {
      // There is no awailable Sym 2 job.
      // Create new Sym 2 job.
      $request_array = array(
        'Name' => $this->title,
        'DueDate' => $this->dueDate,
        'ProjectId' => $this->project,
        'SourceLanguageCode' => $this->sourceLanguageCode,
        'TargetLanguageCodes' => array($this->targetLanguageCode),
      );

      $json_request = json_encode($request_array);
      $result = tmgmt_moravia_symfonie_api_call($translator, "/api/Jobs", "POST", $json_request);

      // Change the Sym 2 job status to ordered.
      $data = $result->data;
      $this->id = $data->Id;
      $json_request = json_encode(array("jobCommand" => "Order"));
      tmgmt_moravia_symfonie_api_call($translator, "/api/Jobs({$this->id})/Default.ExecuteJobCommand", "POST", $json_request);

      // Write the node-Sym 2 job relation to the table for later use.
      $items = $job->getItems();
      if (count($items) == 1) {
        $item = reset($items);
        db_query("REPLACE INTO moravia_two_external_pairs VALUES (:id, :type, :job_id, :p_id) ", array(
            ':id' => $item->item_id,
            ':type' => $item->item_type,
            ':job_id' => $this->id,
            ':p_id' => $job->settings['moravia_project'],
        ));
      }
    }

    // Upload the HTML files of all Drupal job nodes and its elements.
    $items = $job->getItems();
    if ($items) {
      foreach ($items as $item) {
        $data = array_filter(tmgmt_flatten_data($item->getData()), '_tmgmt_filter_data');
        foreach ($data as $key => $element) {
          $value = $element['#text'];
          $file_name = tmgmt_moravia_create_file_name($item->tjiid, $job->label, $key, $this->targetLanguageCode);

          $response = tmgmt_moravia_upload_xliff($translator, $this->id, $file_name, $value, $this->targetLanguageCode);

          // Log fail messages.
          if ($response->code != 201) {
            watchdog('tmgmt_moravia', $file_name . ' - XLIFF upload failed!', array(), WATCHDOG_ERROR);
            $job->addMessage($file_name . ' - XLIFF upload failed!');
            drupal_set_message($file_name . ' - XLIFF upload failed!', 'error');
          }
        }
      }
    }

    // Add due date to instructions.
    $request_array = array();
    $request_array['Id'] = $this->id;
    $request_array['Description'] = format_string("@desc<br/>@lang_code: @date UTC", array("@desc" => $desc, '@lang_code' => $this->targetLanguageCode, '@date' => $this->dueDateEn));
    tmgmt_moravia_symfonie_api_call($translator, "/api/Jobs({$this->id})", "PATCH", json_encode($request_array));
  }

}
