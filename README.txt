TMGMT Translator Moravia 2
--------------------------
The Moravia Translator is a plugin for Drupal’s Translation Management Module.
It connects Drupal with Symfonie 2 (Moravia localization project management
platform) and you can send requests for translations to Moravia.

With this CMS integration, Moravia provides a complete language solution
which covers the full range of activities related to bringing localized
content to your target audience quickly, efficiently, cost-effectively
and with the required quality.

Based on https://www.drupal.org/project/tmgmt_oht
 
FEATURES
--------
 * Translate the content of your site with professional translators
		from Moravia including full quality review
 * Translation into more than 100 languages quickly and efficiently
 
 
REQUIREMENTS
------------
 * This module requires the TMGMT (http://drupal.org/project/tmgmt)
	module to be installed on your Drupal server
 * Moravia will create you a Symfonie account
 * You will generate your user token in Symfonie which you will use in your
		Drupal server /
		Configuration /
		Translator Management Translators /
		Moravia Translator
 * You are ready to start sending jobs to Moravia Symfonie now
 * PHAR support


QUESTIONS?
----------
Contact us at support@moravia.com.


REPOSITORY
------------
git clone moravia@git.drupal.org:sandbox/moravia/2456315.git
