<?php
/**
 * @file
 * File for TMGMT Moravia translator uploading file to Symfonie.
 *
 * Url: http://www.moravia.com/
 */

/**
 * Method to upload attached file to the sym 2 job.
 *
 * @param TMGMTTranslator $translator
 *   Instance of translator.
 * @param int $job_id
 *   Id of job in sym 2.
 * @param string $filename
 *   Name of file to translate.
 * @param string $file_content
 *   Content of file to translate.
 *
 * @return object
 *   Drupal HTTP Request
 */
function tmgmt_moravia_upload_xliff(TMGMTTranslator $translator, $job_id, $filename, $file_content, $lang_code) {
  $url = "https://projects.moravia.com/api/JobAttachments";
  $boundary = '-------------------------acebdf13572468';
  $data = '--' . $boundary . "\r\n" .
      'Content-Disposition: form-data; name=json' . "\r\n" .
      'Content-Type: application/json; charset=UTF-8' . "\r\n\r\n" .
      '{"JobId":' . $job_id . ',Name:"' . $filename . '","FileType":"Source", "TargetLanguageCode":"' . $lang_code . '"}' . "\r\n" .
      '--' . $boundary . "\r\n" .
      'Content-Disposition: form-data; name="file"; filename="' . $filename . '"' . "\r\n" .
      'Content-Type: text/plain' . "\r\n\r\n" .
      $file_content . "\r\n" .
      '--' . $boundary . '--' . "\r\n";

  $ret = drupal_http_request($url, array(
      'method' => 'POST',
      'data' => $data,
      'headers' => array(
        'Content-Type' => 'multipart/form-data; boundary=' . $boundary,
        'Authorization' => 'authToken ' . $translator->getSetting('symfonie_two_token'),
        'Host' => 'projects.moravia.com',
        'Content-Length' => strlen($data),
      ),
  ));

  return $ret;
}
