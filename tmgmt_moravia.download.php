<?php

/**
 * @file
 * File with downloading functions.
 *
 * Url: http://www.moravia.com/
 */

require_once 'tmgmt_moravia.plugin.php';

/**
 * Add comment to Symfonie 2 job.
 *
 * @param TMGMTTranslator $translator
 *   Instance of translator.
 * @param int $job_id
 *   Id of translate job.
 * @param string $message
 *   Content of comment.
 */
function tmgmt_moravia_add_comment(TMGMTTranslator $translator, $job_id, $message) {
  $request_array = array();
  $request_array['JobId'] = $job_id;
  $request_array['Message'] = $message;

  tmgmt_moravia_symfonie_api_call($translator, "/api/JobComments", "POST", json_encode($request_array));
}

/**
 * Creates the FAIL comment message from Symfonie 2 job.
 *
 * @param string $job_lang
 *   Language of job.
 *
 * @return string
 *   Fail comment
 */
function tmgmt_moravia_fail_comment($job_lang) {
  return t("FAIL: [@job_lang]: Upload to Drupal failed. Please re-open the last task in related language workflow, upload finalised files again, complete and approve the task again. This will repeat automatic upload to Drupal.xxx", array('@job_lang' => $job_lang));
}

/**
 * Creates the FAIL message.
 *
 * @return string
 *   Fail message
 */
function tmgmt_moravia_fail_msg() {
  return t("Publish to Drupal was not successful. Files are corrupted or wrong, upload correct files and complete the task again.");
}

/**
 * Check the state of Drupal job. True if pending state.
 *
 * @param TMGMTJob $job
 *   Instance of TMGMTJob.
 *
 * @return bool
 *   If state is active
 */
function tmgmt_moravia_is_pending_job(TMGMTJob $job) {
  return $job->state == TMGMT_JOB_STATE_ACTIVE;
}

/**
 * Checks if the Job is ready. It downloads and import translated files.
 *
 * @param TMGMTJob $job
 *   Instance of TMGMTJob.
 * @param object $languages
 *   Request result.
 * @param object $translator
 *   Translator instance from TMGMT.
 * @param array $job_cache
 *   Cache of jobs.
 * @param TMGMTMoraviaPluginController $controller
 *   Controller of TMGMT plugin.
 */
function tmgmt_moravia_download_job(TMGMTJob $job, $languages, $translator, array &$job_cache, TMGMTMoraviaPluginController $controller) {
  // Check cache to reduce count of the API calls.
  if (array_key_exists($job->reference, $job_cache)) {
    $ex_job_attach_data = $job_cache[$job->reference];
  }
  else {
    // If not cched, gets all Delivery files of the Symfonie 2 job.
    $ex_job_attach = tmgmt_moravia_symfonie_api_call(
      $translator,
      '/api/JobAttachments?$filter=' . urlencode('JobId eq ' . $job->reference . ' and FileType eq Moravia.Symfonie.Data.FileType\'Target\''),
      "GET"
    );

    if (isset($ex_job_attach->error)) {
      return;
    }

    $ex_job_attach_data = $ex_job_attach->data;
    $job_cache[$job->reference] = $ex_job_attach_data;
  }

  // Get the language name.
  $job_lang = 'Undefined';
  if (isset($languages->value) && $languages->value) {
    foreach ($languages->value as $lang) {
      if ($lang->Ietf == $job->target_language) {
        $job_lang = $lang->Name;
      }
    }
  }

  /*
   * Go through all Drupal items, check if there is the translated file
   * and if yes, download the translated content.
   */
  $items = $job->getItems();
  $translated_data = array();
  $received_files = array();
  if ($items) {
    foreach ($items as $item) {
      $data = array_filter(tmgmt_flatten_data($item->getData()), '_tmgmt_filter_data');
      // Go through all node attributes as title, body, etc.
      foreach ($data as $key => $element) {
        // Create the file name of the translated file, we are looking for.
        $file_name = tmgmt_moravia_create_file_name($item->tjiid, $job->label, $key, (string) $translator->settings['remote_languages_mappings'][$job->target_language]);

        // Go through all Symfonie 2 job delivery files.
        foreach ($ex_job_attach_data->value as $file) {
          /*
           * If the delivery file name matches the attribute file name,
           * download its content.
           */
          if ($file->Name == $file_name) {
            $html_data = tmgmt_moravia_symfonie_api_call($job->getTranslator(), '/api/' . $file->DownloadUrl, "GET", NULL, FALSE);

            $doc = $html_data->data;

            if ($doc) {
              // Creates the node translated data to import it by TMGMT method.
              $translated_data[$item->tjiid . "][" . $key]['#text'] = $doc;
              $received_files[] = $file_name;
            }
            else {
              $job->addMessage(tmgmt_moravia_fail_msg());
              tmgmt_moravia_add_comment($translator, $job->reference, tmgmt_moravia_fail_comment($job_lang));
            }
          }
        }
      }
    }
  }

  if ($translated_data) {
    // Import downloaded contents to the Drupal job item.
    $translated_data = tmgmt_unflatten_data($translated_data);
    array_walk_recursive($translated_data, 'tmgmt_moravia_unescape_translation');
    $job->addTranslatedData($translated_data);

    // Write log and comment messages.
    $job->addMessage('The translation has been received (' . implode(', ', $received_files) . ').');
    tmgmt_moravia_add_comment($translator, $job->reference, format_string ("SUCCESS: [@job_lang]: Finalised file was successfully uploaded to Drupal (@files).", array('@job_lang' => $job_lang, '@files' => implode(', ', $received_files))));
  }
  else {
    $job->addMessage(tmgmt_moravia_fail_msg());
    tmgmt_moravia_add_comment($translator, $job->reference, tmgmt_moravia_fail_comment($job_lang));
  }
}

/**
 * Decodes the string HTML entities.
 *
 * @param string $value
 *   String to decode.
 */
function tmgmt_moravia_unescape_translation(&$value) {
  if (is_string($value)) {
    $value = html_entity_decode($value);
  }
}

/**
 * Main CRON method to check all open Drupal jobs to download translations.
 */
function tmgmt_moravia_download() {
  // Get all moravia_two translators.
  $result = db_query("SELECT * FROM tmgmt_translator WHERE plugin='moravia_two'", array());
  $moravia_translators = $result->fetchAllAssoc('tid');
  $job_cache = array();

  foreach ($moravia_translators as $translator) {
    // Get all translator jobs.
    $query = new EntityFieldQuery();
    $moravia_pending_jobs = $query
      ->entityCondition('entity_type', 'tmgmt_job')
      ->propertyCondition('translator', $translator->name)
      ->execute();

    if (!array_key_exists('tmgmt_job', $moravia_pending_jobs)) {
      return;
    }

    $ids = array();

    foreach ($moravia_pending_jobs['tmgmt_job'] as $pj) {
      $ids[] = $pj->tjid;
    }

    if (count($ids) < 0) {
      continue;
    }

    // Load all job by their ids.
    $controller = tmgmt_translator_plugin_controller("moravia_two");
    $jobs = entity_load('tmgmt_job', $ids);
    $languages = NULL;
    $translator = NULL;

    // Try to import all jobs.
    foreach ($jobs as $job) {
      // If non-active job, lets continue.
      if (!tmgmt_moravia_is_pending_job($job)) {
        continue;
      }

      // Get the translator object (only once).
      if (!$translator) {
        /* @noinspection PhpUndefinedMethodInspection */
        $translator = $job->getTranslator();
      }

      // Get all languages info (only once).
      if (!$languages) {
        $lang_res = tmgmt_moravia_symfonie_api_call($translator, '/api/Languages', "GET");
        if (isset($lang_res->error)) {
          drupal_set_message(t('moravia_symfonie_two: @ermsg /api/Languages', array('@ermsg' => $lang_res->error)), 'error');
          return;
        }
        $languages = $lang_res->data;
      }
      // Try to import one job data.
      tmgmt_moravia_download_job($job, $languages, $translator, $job_cache, $controller);
    }
  }
}
